# Call libraries
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.slider import Slider
from kivy.uix.widget import Widget
from kivy.uix.gridlayout import GridLayout
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.properties import ObjectProperty, NumericProperty, StringProperty, BooleanProperty
import numpy as np
import matplotlib
import serial
import pygame
import cv2

# Initialize libraries
pygame.init()
pygame.joystick.init()

# define variables needed later on
In_Min = float(-1)
In_Max = float(1)
Out_Min = float(1000)
Out_Max = float(2000)
Scaled_Out_Min = float(1250)
Scaled_Out_Max = float(1750)

Start_Marker = '<'
End_Marker = '\n'
Dividing_Marker = ','
port = 'COM12'
ard = serial.Serial(port, 115200, timeout = 5)

# define kv file used for positioning of widgets
Builder.load_string('''
<tabpanelkv>:
    size_hint: 1, 1
    pos_hint: {'center_x': .5, 'center_y': .5}
    do_default_tab: False
    TabbedPanelItem:
        text: 'Cameras'
        GridLayout:
            rows: 1
            cols: 2
            GridLayout:
                rows: 2
                cols: 1
                Button:
                    text: "Help"
                GridLayout:
                    rows: 1
                    cols: 5
                    size_hint: .2, .2
                    Button:
                        text: "Horizontal Motor Data"
                    Button:
                        text: "Vertical Motor Data"
                    Button:
                        text: "Camera Position Data"
                    Button:
                        text: "Arm Position Data"
                    Button:
                        text: "Lights On/Off Data"
            GridLayout:
                rows: 4
                cols: 1
                size_hint: .3, .3
                Button:
                    text: "Cam Place Holder"
                    size_hint: .3, .3
                Button:
                    text: "Cam Place Holder"
                    size_hint: .3, .3
                Button:
                    text: "Cam Place Holder"
                    size_hint: .3, .3
                Button:
                    text: "Camera Location Switch"
                    size_hint: .3, .3
    TabbedPanelItem:
        text: 'Autonomous'
        GridLayout:
            rows: 1
            cols: 2
            GridLayout:
                rows: 2
                cols: 1
                Button:
                    text: "Animation to show completion of mission"
                GridLayout:
                    rows: 1
                    cols: 3
                    size_hint: .2, .2
                    Button:
                        text: "Start Subway Mission"
                    Button:
                        text: "Start Transect Mission"
                    Button:
                        text: "Start Coral Mission"
            GridLayout:
                rows: 6
                cols: 2
                size_hint: .2, .2
                Slider:
                    id: slider_1
                    min: 0
                    max: 1
                    step: 0.01
                    orientation: 'horizontal'
                    size_hint: 1, 1
                Label:
                    text: str(round(slider_1.value, 2))
                    size_hint: .3, .3
                Slider:
                    id: slider_2
                    min: 0
                    max: 1
                    step: 0.01
                    orientation: 'horizontal'
                    size_hint: 1, 1
                Label:
                    text: str(round(slider_2.value, 2))
                    size_hint: .3, .3
                Slider:
                    id: slider_3
                    min: 0
                    max: 1
                    step: 0.01
                    orientation: 'horizontal'
                    size_hint: 1, 1
                Label:
                    text: str(round(slider_3.value, 2))
                    size_hint: .3, .3
                Slider:
                    id: slider_4
                    min: 0
                    max: 1
                    step: 0.01
                    orientation: 'horizontal'
                    size_hint: 1, 1
                Label:
                    text: str(round(slider_4.value, 2))
                    size_hint: .3, .3
                Slider:
                    id: slider_5
                    min: 0
                    max: 1
                    step: 0.01
                    orientation: 'horizontal'
                    size_hint: 1, 1
                Label:
                    text: str(round(slider_5.value, 2))
                    size_hint: .3, .3
                Slider:
                    id: slider_6
                    min: 0
                    max: 1
                    step: 0.01
                    orientation: 'horizontal'
                    size_hint: 1, 1
                Label:
                    text: str(round(slider_6.value, 2))
                    size_hint: .3, .3
''')

# define the tabpanelkv class to create the necessary tabbed widget
class tabpanelkv(TabbedPanel):
    pass

# Create the 4 Camera classes to allow for positioning and data extraction from cameras
class kivyCamera1(Image):
    cam = ObjectProperty()
    fps = NumericProperty(30)

    def __init__(self, **kwargs):
        super(kivyCamera1, self).__init__(**kwargs)
        self._capture = None
        if self.cam is not None:
            self._capture = cv2.VideoCapture(self.cam)
        Clock.schedule_interval(self.update, 1.0 / self.fps)

    def on_cam(self, *args):
        if self._capture is not None:
            self._capture.release()
        self._capture = cv2.VideoCapture(self.cam)

    @property
    def capture(self):
        return self._capture

    def update(self, dt):
        ret, frame = self.capture.read()
        if ret:
            buf1 = cv2.flip(frame, 0)
            buf2 = cv2.resize(buf1, (1280, 720))
            buf = buf2.tostring()
            image_texture = Texture.create(size = (buf2.shape[1], buf2.shape[0]), colorfmt = "bgr")
            image_texture.blit_buffer(buf, colorfmt = "bgr", bufferfmt = "ubyte")
            self.texture = image_texture

class kivyCamera2(Image):
    cam = ObjectProperty()
    fps = NumericProperty(30)

    def __init__(self, **kwargs):
        super(kivyCamera2, self).__init__(**kwargs)
        self._capture = None
        if self.cam is not None:
            self._capture = cv2.VideoCapture(self.cam)
        Clock.schedule_interval(self.update, 1.0 / self.fps)

    def on_cam(self, *args):
        if self._capture is not None:
            self._capture.release()
        self._capture = cv2.VideoCapture(self.cam)

    @property
    def capture(self):
        return self._capture

    def update(self, dt):
        ret, frame = self.capture.read()
        if ret:
            buf1 = cv2.flip(frame, 0)
            buf2 = cv2.resize(buf1, (1280, 720))
            buf = buf2.tostring()
            image_texture = Texture.create(size = (buf2.shape[1], buf2.shape[0]), colorfmt = "bgr")
            image_texture.blit_buffer(buf, colorfmt = "bgr", bufferfmt = "ubyte")
            self.texture = image_texture

class kivyCamera3(Image):
    cam = ObjectProperty()
    fps = NumericProperty(30)

    def __init__(self, **kwargs):
        super(kivyCamera3, self).__init__(**kwargs)
        self._capture = None
        if self.cam is not None:
            self._capture = cv2.VideoCapture(self.cam)
        Clock.schedule_interval(self.update, 1.0 / self.fps)

    def on_cam(self, *args):
        if self._capture is not None:
            self._capture.release()
        self._capture = cv2.VideoCapture(self.cam)

    @property
    def capture(self):
        return self._capture

    def update(self, dt):
        ret, frame = self.capture.read()
        if ret:
            buf1 = cv2.flip(frame, 0)
            buf2 = cv2.resize(buf1, (1280, 720))
            buf = buf2.tostring()
            image_texture = Texture.create(size = (buf2.shape[1], buf2.shape[0]), colorfmt = "bgr")
            image_texture.blit_buffer(buf, colorfmt = "bgr", bufferfmt = "ubyte")
            self.texture = image_texture

class kivyCamera4(Image):
    cam = ObjectProperty()
    fps = NumericProperty(30)

    def __init__(self, **kwargs):
        super(kivyCamera4, self).__init__(**kwargs)
        self._capture = None
        if self.cam is not None:
            self._capture = cv2.VideoCapture(self.cam)
        Clock.schedule_interval(self.update, 1.0 / self.fps)

    def on_cam(self, *args):
        if self._capture is not None:
            self._capture.release()
        self._capture = cv2.VideoCapture(self.cam)

    @property
    def capture(self):
        return self._capture

    def update(self, dt):
        ret, frame = self.capture.read()
        if ret:
            buf1 = cv2.flip(frame, 0)
            buf2 = cv2.resize(buf1, (1280, 720))
            buf = buf2.tostring()
            image_texture = Texture.create(size = (buf2.shape[1], buf2.shape[0]), colorfmt = "bgr")
            image_texture.blit_buffer(buf, colorfmt = "bgr", bufferfmt = "ubyte")
            self.texture = image_texture

# Create the joystick class to handle input from 2 logitech joysticks
class joystickClass(GridLayout, Label):
    def __init__(self, **kwargs):
        super(joystickClass, self).__init__(**kwargs)
        self.Joy1 = pygame.joystick.Joystick(0)
        self.Joy2 = pygame.joystick.Joystick(1)

        self.Joy1.init()
        self.Joy2.init()

        self.lights = 1
        self.Xmov = 1500
        self.Ymov = 1500
        self.Zmov = 1500
        self.User = 1500
        self.Tser = 1500
        self.CTser = 1500
        self.Oser = 1500
        Clock.schedule_interval(self.JoyRead, 1 / 30)

    def JoyRead(self, dt):
        for event in pygame.event.get():
            self.Joy1X = self.Joy1.get_axis(0)
            self.Joy1Y = self.Joy1.get_axis(1)
            self.Joy2X = self.Joy2.get_axis(0)
            self.Joy2Y = self.Joy2.get_axis(1)

            self.Joy1B2 = self.Joy1.get_button(1)
            self.Joy1B3 = self.Joy1.get_button(2)
            self.Joy1B6 = self.Joy1.get_button(5)
            self.Joy1B7 = self.Joy1.get_button(6)
            self.Joy1B8 = self.Joy1.get_button(7)
            self.Joy1B9 = self.Joy1.get_button(8)
            self.Joy1B10 = self.Joy1.get_button(9)

            self.Joy2B1 = self.Joy2.get_button(0)
            self.Joy1B6 = self.Joy2.get_button(5)

            self.Joy1H = self.Joy1.get_hat(0)
            if self.Joy1B10 == True:
                self.Xmov = (((self.Joy1X - In_Min) * (Scaled_Out_Max - Scaled_Out_Min)) / (In_Max - In_Min)) + 1250
                self.Ymov = 1250 + (((self.Joy1Y - In_Min) * (Scaled_Out_Max - Scaled_Out_Min)) / (In_Max - In_Min))
            else:
                self.Xmov = (((self.Joy1X - In_Min) * (Out_Max - Out_Min)) / (In_Max - In_Min)) + 1000
                self.Ymov = 1000 - (((self.Joy1Y - In_Min) * (Out_Max - Out_Min)) / (In_Max - In_Min)) + 1000

            self.User = 1000 - (((self.Joy1Y - In_Min) * (Out_Max - Out_Min)) / (In_Max - In_Min)) + 1000
            self.Tser = (((self.Joy2X - In_Min) * (Out_Max - Out_Min)) / (In_Max - In_Min)) + 1000

            if self.Joy1B2 == True:
                self.CTser = 1500
            if self.Joy1B9 == True:
                self.Zmov = 2000
            elif self.Joy1B10 == True:
                self.Zmov = 1000
            else:
                self.Zmov = 1500
            if self.Joy1H == (0, 1):
                self.CTser += 10
            elif self.Joy1H == (0, -1):
                self.CTser -= 10
            if self.Joy2B1 == True:
                self.Oser = 2000
            else:
                self.Oser = 1000
            if self.Joy1B6 == True:
                self.lights ^= 1

    # Create the Arduino Class
    def Arduino(self, dt):
        stringsending = str(round(self.Xmov))
        stringsending += dividingmarker
        stringsending += str(round(self.Ymov))
        stringsending += dividingmarker
        stringsending += str(round(self.Zmov))
        stringsending += dividingmarker
        stringsending += str(round(self.User))
        stringsending += dividingmarker
        stringsending += str(round(self.Tser))
        stringsending += dividingmarker
        stringsending += str(round(self.Oser))
        stringsending += dividingmarker
        stringsending += str(round(self.CTser))
        stringsending += dividingmarker
        stringsending += str(round(self.lights))
        print(stringsending)

        if ard.inWaiting():
            ard.write(stringsending)
            returnmsg = ard.read()
            print(returnmsg)

# Create the Transect Algorithm Class
class transect_Alg(GridLayout, Image):
    pass

# Create the Subway Algorithm Class
class subway_Alg(GridLayout, Image):
    pass

# Create the Mussels Class
class mussels_Alg(GridLayout, Image):
    pass

# Create the main class which calls the tabpanelkv class which in turn calls the other classes
class MainClass(App, GridLayout):
    def build(self):
        return tabpanelkv()

# Create the looping function that terminates when either the red x or esc is pressed
if __name__ = '__main__':
    MainClass().run()
